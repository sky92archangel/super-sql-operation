﻿using SuperSqlOperation.SqlServer;

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperSqlOperation
{
    public static partial class SqlServerDataAsync
    {
        #region 增加表格  create
        public static async Task<int> CreateAsync<T>()
        {
            using (IDbConnection conn = SqlHelper.ConnFactory.GetConnection())
            {
                return await conn.CreateExecute<T>();
            }
        }
        #endregion

        #region 插入数据 insert

        /// <summary>
        /// 异步 插入数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="model"></param>
        /// <returns></returns>
        public static async Task<int> InsertAsync<T>(T model) where T : class
        {
            using (IDbConnection conn = SqlHelper.ConnFactory.GetConnection())
            {
                return await conn.InsertExecute<T>(model);
            }
        }

        /// <summary>
        /// 批量 异步 插入数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="models"></param>
        /// <returns></returns>
        public static async Task<IEnumerable<int>> InsertAsync<T>(IEnumerable<T> models) where T : class
        {
            using (IDbConnection conn = SqlHelper.ConnFactory.GetConnection())
            {
                return await conn.InsertExecute<T>(models);
            }
        }

        /// <summary>
        /// 同步 插入数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="model"></param>
        /// <returns></returns>
        public static Task<int> Insert<T>(T model) where T : class
        {
            using (IDbConnection conn = SqlHelper.ConnFactory.GetConnection())
            {
                return conn.InsertExecute<T>(model);
            }
        }

        #endregion

        #region 查询数据 select

        /// <summary>
        /// 根据条件进行筛选 默认全表查询
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static async Task<IEnumerable<T>> GetAsync<T>() where T : class, new()
        {
            using (IDbConnection conn = SqlHelper.ConnFactory.GetConnection())
            {
                return await conn.SelectExecute<T>();
            }
        }

        /// <summary>
        /// 根据条件进行筛选  按主键id查询
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        /// <returns></returns>
        public static async Task<IEnumerable<T>> GetAsync<T>(int id) where T : class
        {
            using (IDbConnection conn = SqlHelper.ConnFactory.GetConnection())
            {
                return await conn.SelectExecute<T>(id);
            }
        }

        /// <summary>
        /// 根据条件进行筛选   若条件空  则默认全表查询  默认全条件和
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="conds"></param>
        /// <returns></returns>
        //public static async Task<IEnumerable<T>> GetAsync<T>(Dictionary<string, object> conds) where T : class 
        //{
        //    using (IDbConnection conn = SqlHelper.ConnFactory.GetConnection())
        //    {
        //        if (conds.Count != 0)
        //            return await conn.SelectExecute<T>(conds);                   
        //        else
        //            return await conn.SelectExecute<T>( );
        //    }
        //}

        /// <summary>
        /// 根据条件进行筛选   若条件空  则默认全表查询  默认并
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="conds">查询条件</param>
        /// <param name="linkStr">可选项 ： AND OR  </param>
        /// <returns></returns>
        public static async Task<IEnumerable<T>> GetAsync<T>(Dictionary<string, object> conds, string linkStr = "AND") where T : class
        {
            using (IDbConnection conn = SqlHelper.ConnFactory.GetConnection())
            {
                if (conds.Count != 0)
                    return await conn.SelectExecute<T>(conds, linkStr);
                else
                    return await conn.SelectExecute<T>();
            }
        }
        #endregion

        #region 修改数据 update

        public static async Task<int> UpdateAsync<T>(Dictionary<string, object> columns, Dictionary<string, object> conds, string linkStr = "AND") where T : class
        {
            using (IDbConnection conn = SqlHelper.ConnFactory.GetConnection())
            {
                return await conn.UpdateExecute<T>(columns, conds, linkStr);
            }
        }
        public static async Task<int> UpdateAsync<T>(T t, Dictionary<string, object> conds, string linkStr = "AND") where T : class
        {
            using (IDbConnection conn = SqlHelper.ConnFactory.GetConnection())
            {
                return await conn.UpdateExecute<T>(t, conds, linkStr);
            }
        }

        #endregion

        #region 删除数据 delete

        /// <summary>
        /// 删除某一数据  异步
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        /// <returns></returns>
        public static async Task<int> DeleteAsync<T>(T t) where T : class
        {
            using (IDbConnection conn = SqlHelper.ConnFactory.GetConnection())
            {
                return await conn.DeleteExecute<T>(t);
            }
        }

        public static async Task<int> DeleteAsync<T>(T t, Dictionary<string, object> conds, string linkStr = "AND") where T : class
        {
            using (IDbConnection conn = SqlHelper.ConnFactory.GetConnection())
            {
                if (conds.Count != 0)
                    return await conn.DeleteExecute<T>(t, conds, linkStr);
                else
                    return await conn.DeleteExecute<T>(t);
            }
        }

        /// <summary>
        /// 全表删除  异步
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        /// <returns></returns>
        public static async Task<int> ClearAsync<T>( ) where T : class
        {
            using (IDbConnection conn = SqlHelper.ConnFactory.GetConnection())
            {
                return await conn.ClearExecute<T>( );
            }
        }
        #endregion

    }

    #region 使用方法
    /*
     * 使用方法
     使用方法
    //====================
    //定义类 TestClass  
    [Table("table_User")]  //数据库表名称
    public class TestClass
    {
        [Key] //主键特性
        public int u_Uid { get; set; }
        public string u_Name { get; set; }
        public string u_Password { get; set; }
        public string u_Dept { get; set; }
        public string u_Premission { get; set; } 
        [Write(false)]  //不进行读写特性
        public string foo { get; set; }
        [Ignore(true)]  //不进行读写特性
        public string  fee { get; set; }
    }
    //====================

     //====================
    //初始化连接
     SqlHelper sqlHelper 
        = SqlHelper.GetInstanceServer(@"Server=10.193.217.38;Database=DATABASE_NAME;Uid=sa;Pwd=Password");
    //====================

    //创建实例
    TestClass testClass = new TestClass();
    //赋值...
    testClass.u_Uid = 50;
    testClass.u_Name = "u_Name2";
    testClass.u_Password = "u_Password2";
    testClass.u_Dept = "u_Dept2";
    testClass.u_Premission = "u_Premission2";
    testClass.foo = "foo2";
    testClass.fee = "fee2";
    
    //====================
    //创建表
    var rescr = SqlServerDataAsync.CreateAsync(new TestClass()).Result; 
    //====================
    // 插入 insert
    int resi = SqlServerDataAsync.InsertAsync(testClass).Result ;   
     
    //====================
    /// 修改 
    Dictionary<string, object> columns = new Dictionary<string, object>();  //标记为操作字段
    columns.Add("u_Name", "EEE");
    columns.Add("u_Password", "FFF");
    Dictionary<string, object> conds = new Dictionary<string, object>();     //查找条件字段
    conds.Add("u_Uid", 50);  
    
    int res = SqlServerDataAsync.UpdateAsync<TestClass>(columns,conds).Result;       //对查找结果修改标记操作字段
    int res2 = SqlServerDataAsync.UpdateAsync<TestClass>(testClass2, conds).Result;  //对查找结果替换所有字段
    //==================== 
    //查询
    List<TestClass> tClasss1 = SqlServerDataAsync.GetAsync<TestClass>(conds, "OR").Result.ToList(); //查找符合条件数据中的标记操作字段
    List<TestClass> tClasss2 = SqlServerDataAsync.GetAsync<TestClass>( ).Result.ToList(); //全表查询
    //==================== 

     //====================
     //删除 delete
     int resd = SqlServerDataAsync.DeleteAsync<TestClass>(testClass).Result;
     //清空
     int rescl = SqlServerDataAsync.ClearAsync<TestClass>(testClass).Result;
     //====================


     */
    #endregion

}
