﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SuperSqlOperation
{
    public static class SqlTypeFunc
    {
        public static string GetSqlType(this PropertyInfo property)
        {
            string typeStr = "TEXT";

            Type type = property.PropertyType;
            if (type.Equals(typeof(int))) return "INTEGER";
            if (type.Equals(typeof(bool))) return "BIT";
            if (type.Equals(typeof(string))) return "TEXT";
            if (type.Equals(typeof(double))) return "REAL";
            if (type.Equals(typeof(float))) return "REAL";
            if (type.Equals(typeof(DateTime))) return "DATETIME";

            return typeStr;
        }



        //public static  K ConvertBy<T,K>(this T objTarget ,K objSource)  where K : new() 
        //{
        //     K newObj = new K();
        //     newObj = (K)objTarget; 
        //     return newObj; 
        // }
         
        public static object ConverInt(this object objTarget)
        { 
            if (objTarget.GetType().Equals(typeof(Int64)))
            { return Int32.Parse(objTarget.ToString()); }
            else { return objTarget; }
        }

        public static object ConvertObject( object objTarget , PropertyInfo propertyInfo)
        {
            Type propType  = propertyInfo.PropertyType;
            Type objType = objTarget.GetType();
            //类型一样 则返回
            if ( objType.Equals(propType)) return objTarget; 
            else if (objType.Equals(typeof(Int64))) return Int32.Parse(objTarget.ToString());
              
            return objTarget;
        }
    }
}
