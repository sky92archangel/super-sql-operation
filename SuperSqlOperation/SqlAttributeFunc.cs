﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SuperSqlOperation
{

    /// <summary>
    /// 属性检查   
    /// </summary>
    static class SqlAttributeFunc
    {
        /// <summary>
        /// 通过特性获取类对应的数据库表的名称
        /// </summary>
        /// <param name="attribute"></param>
        /// <returns></returns>
        public static string GetDataTableNameByAttr(this Type t)
        {
            string tableName = string.Empty;
            Attribute[] attribute = Attribute.GetCustomAttributes(t);
            if (attribute.Length == 0)
            {
                tableName = t.Name;
            }
            else
            {
                foreach (Attribute attr in attribute)
                {
                    Table dba = attr as Table;
                    if (dba == null) continue;
                    tableName = dba.TableName;
                    break;
                }
            }
            return tableName;
        }

        /// <summary>
        /// 是否有主键特性
        /// </summary>
        /// <param name="prop"></param>
        /// <returns></returns>
        public static bool GetDataKeyByAttr(this PropertyInfo prop)
        {
            foreach (Attribute attr in prop.GetCustomAttributes(true))
            {
                Key key = attr as Key;
                if (key == null) continue;
                return true;
            }
            return false;
        }

        /// <summary>
        ///  write特性是否可写
        /// </summary>
        /// <param name="prop"></param>
        /// <returns></returns>
        public static bool GetDataWriteByAttr(this PropertyInfo prop)
        {
            foreach (Attribute attr in prop.GetCustomAttributes(true))
            {
                Write write = attr as Write;
                if (write == null) continue;
                return write.IsWrite;
            }
            return true;
        }

        /// <summary>
        /// 是否具有忽略选项
        /// </summary>
        /// <param name="prop"></param>
        /// <returns></returns>
        public static bool GetDataIgnoreByAttr(this PropertyInfo prop)
        {
            foreach (Attribute attr in prop.GetCustomAttributes(true))
            {
                Ignore ignore = attr as Ignore;
                if (ignore == null) continue;
                return ignore.IsIgnore;
            }
            return false;
        }

        /// <summary>
        /// 自定义列名
        /// </summary>
        /// <param name="prop"></param>
        /// <returns></returns>
        public static string GetDataColumnByAttr(this PropertyInfo prop)
        {
            string columnName = prop.Name;
            foreach (Attribute attr in prop.GetCustomAttributes(true))
            {
                Column column = attr as Column;
                if (column == null) continue;
                return column.ColumnName;
            }
            return columnName;
        }
    }

}
