﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperSqlOperation
{ 
    [AttributeUsage(AttributeTargets.Property)]
    public class Key : Attribute
    {
        public Key () { } 
    }
     
    [AttributeUsage(AttributeTargets.Property)]
    public class Write : Attribute
    {
        private bool isWrite = true;
        public Write(bool _isWrite)
        {
            isWrite = _isWrite;
        }
        public bool IsWrite { get { return isWrite; } }
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class Ignore : Attribute
    {
        private bool isIgnore = false;
        public Ignore(bool _isIgnore)
        {
            isIgnore = _isIgnore;
        }
        public bool IsIgnore { get { return isIgnore; } }
    }
     
    [AttributeUsage(AttributeTargets.Class)]
    public class Table : Attribute
    {
        private string tableName = string.Empty;
        public Table(string _tableName)
        { tableName = _tableName; }
        public string TableName { get { return tableName; } }
    }
     
    [AttributeUsage(AttributeTargets.Property)]
    public class Column : Attribute
    {
        private string column = string.Empty;
        public Column(string _column)
        {
            column = _column;
        }
        public string ColumnName { get { return column; } }
    }

}
