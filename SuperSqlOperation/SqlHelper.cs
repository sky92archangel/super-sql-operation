﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SuperSqlOperation
{
    public class SqlHelper
    {
        string connStr;
        private static SqlHelper _singleton;
        private static bool ToServer = false;
        public static void ClearInstance()
        { _singleton = null; }

        //一般构造
        private SqlHelper(string dbPath, bool toServer = false)
        {
            connStr = dbPath;
            ToServer = toServer;
        }
        /// <summary>
        /// 获得sqlite连接
        /// </summary>
        /// <param name="_path"></param>
        /// <param name="isAbsPath"></param>
        /// <returns></returns>
        public static SqlHelper GetInstanceSqlite(string _path, bool isAbsPath = false)
        {
            string dllPath = string.Empty;
            if (!isAbsPath) //不是绝对路径
            { dllPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location); }
            if (_singleton == null)
            {
                string localRoomDB = @"Data Source = " + dllPath + _path + "; Version=3;Compress=False;";
                _singleton = new SqlHelper(localRoomDB);
                _singleton.connStr = localRoomDB;
            }
            return _singleton;
        }
        /// <summary>
        /// 获得数据库连接
        /// </summary>
        /// <param name="_server"></param>
        /// <returns></returns>
        public static SqlHelper GetInstanceServer(string _server)
        {
            string server = _server;
            if (_singleton == null)
            {
                //  IDbConnection connection = new System.Data.SqlClient.SqlConnection(server);
                string localRoomDB = server;
                _singleton = new SqlHelper(localRoomDB, true);
                _singleton.connStr = localRoomDB;
            }
            return _singleton;
        }

        public static class ConnFactory
        {
            //public static string connectionString = _singleton.connStr;
            public static IDbConnection GetConnection()
            {
                IDbConnection conn = null;
                if (!ToServer) conn = new System.Data.SQLite.SQLiteConnection(_singleton.connStr);
                if (ToServer) conn = new System.Data.SqlClient.SqlConnection(_singleton.connStr);

                return conn;
            }
        }
    }

    public class SqlServerHelper
    {
        string connStr;
        private static SqlServerHelper _singleton;
     
        public static void ClearInstance()
        { _singleton = null; }

        //一般构造
        private SqlServerHelper(string dbPath )
        {
            connStr = dbPath; 
        }
      
        /// <summary>
        /// 获得数据库连接
        /// </summary>
        /// <param name="_server"></param>
        /// <returns></returns>
        public static SqlServerHelper GetInstanceServer(string _server)
        {
            string server = _server;
            if (_singleton == null)
            {
                //  IDbConnection connection = new System.Data.SqlClient.SqlConnection(server);
                string localRoomDB = server;
                _singleton = new SqlServerHelper(localRoomDB );
                _singleton.connStr = localRoomDB;
            }
            return _singleton;
        }

        public static class ConnFactory
        {
            public static string connectionString = _singleton.connStr;
            public static IDbConnection GetConnection()
            {
                IDbConnection conn = new System.Data.SqlClient.SqlConnection(connectionString);

                return conn;
            }
        }
    }

    public class SqliteHelper
    {
        string connStr;
        private static SqliteHelper _singleton;
       
        public static void ClearInstance()
        { _singleton = null; }

        //一般构造
        private SqliteHelper(string dbPath )
        {
            connStr = dbPath; 
        }

        /// <summary>
        /// 获得sqlite连接
        /// </summary>
        /// <param name="_path"></param>
        /// <param name="isAbsPath"></param>
        /// <returns></returns>
        public static SqliteHelper GetInstanceSqlite(string _path, bool isAbsPath = false)
        {
            string dllPath = string.Empty;
            if (!isAbsPath) //不是绝对路径
            { dllPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location); }
            if (_singleton == null)
            {
                string localRoomDB = @"Data Source = " + dllPath + _path + "; Version=3;Compress=False;";
                _singleton = new SqliteHelper(localRoomDB);
                _singleton.connStr = localRoomDB;
            }
            return _singleton;
        }
        

        public static class ConnFactory
        {
            public static string connectionString = _singleton.connStr;
            public static IDbConnection GetConnection()
            {
                IDbConnection conn = new System.Data.SQLite.SQLiteConnection(connectionString);
               
                return conn;
            }
        }
    }
     
}
